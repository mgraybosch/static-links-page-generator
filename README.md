# README

The "Static Links Page Generator" is both a database and a tool.
You can add a link to your own website to the collection, or use
the existing collection to generate Markdown-formatted links page
files that can be used with the static site generator of your choice
(or WordPress if your site is configured to use Markdown).

## Rationale

The only way we can rebuild the open web is by ourselves, by linking
to interesting sites and getting linked in turn. The hard part is
finding interesting sites. That's what this repository is for.

## How to Contribute

1. Fork and clone this repository.
2. Copy ```templates/link.md``` to the directory under ```links/``` that matches the first letter of your family name.
3. Rename link.md using your family name, personal name, and optionally your general location.
4. Edit your link file using your favorite text editor.
5. Add your file to the repository, commit, push to your remote, and submit a pull request.

## How to Use This Repository

1. Fork and clone this repository.
2. Copy ```templates/page.md``` and edit it using your favorite editor.
3. Run ```lpgen``` by passing the path of your input file, the path of your output file, and a search term.
4. Verify that your output file contains the links you want to share, or edit to taste.
5. Move your finished file to a location where your static site generator will process it.

## Tools Used

I created the "Static Links Page Generator" on OpenBSD 6.4. 
It should work on any Linux or Unix-like system that provides
the following tools:

* /bin/sh
* /bin/test
* /bin/echo
* /bin/cat
* /usr/bin/find
* /usr/bin/grep
* /usr/bin/cut
* /usr/bin/uniq

