## Emma Cooper

Emma Cooper is a gardener, writer, and ethnobotanist. She writes about
plants, gardening, food, and the environment.

### Personal Website 

* [https://theunconventionalgardener.com/](https://theunconventionalgardener.com/)

<!-- search tags: writer, garden, gardening, gardener, food, environment, ethnobotany -->


