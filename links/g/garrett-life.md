## Garrett

Garrett (no last name given) is currently reworking his personal
blog. He writes about cybersecurity, privacy, and Buddhism there. He
also writes for Approaching Utopia, a cyberpunk/solarpunk
publishing collective.

### Personal Website 

* [http://garrett.life](http://garrett.life)

### Other Websites

* [http://approachingutopia.com/](http://approachingutopia.com/)

<!-- search tags: writers, cybersecurity, privacy, buddhism, cyberpunk, solarpunk -->


